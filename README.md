# Async & WebGL

Un exemple simple (?) permettant de faire des choses compliquées :
- éxecution de code asynchrone grâce aux Promise et aux fonctions asynchrone (`async` / `await`)
- affichage d'un canvas webgl en arrière plan (ThreeJS)

Le tout facilement mis en place grâce à l'usage des modules (`import` / `export`)

[live demo](https://jniac-playground.gitlab.io/async-and-webgl)

<img src="screenshots/demo.gif" width="500">
