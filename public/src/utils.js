/**
 * Remplace le contenu "text" (innerText)
 * par autant de <span/> qu'il n'y a de lettres.
 * @param {HTMLDivElement} div 
 */
 export const splitToSpan = (div) => {

    const mapChar = (char) => {
        if (char === ' ') {
            return '&nbsp;'
        }
        if (char === '\n') {
            return '<br>'
        }
        return `<span class="split">${char}</span>`
    }

    div.innerHTML = [...div.innerText]
        .map(c => mapChar(c))
        .join('')
}



/**
 * Suspend l'éxecution d'une fonction asynchrone N secondes.
 * @param {number} seconds 
 * @returns 
 */
export const wait = (seconds) => new Promise(r => setTimeout(r, seconds * 1000))



/**
 * Réalise un clone du tableau, et distribue aléatoirement le contenu.
 * @param {any[]} items 
 */
export const shuffle = (items) => {
    const result = [...items]
    const max = result.length
    for (let index = 0; index < max; index += 1) {
        const randomIndex = Math.floor(Math.random() * max)
        const tmp = result[index]
        result[index] = result[randomIndex]
        result[randomIndex] = tmp
    }
    return result
}

