import { shuffle, wait } from './utils.js'

/**
 * Réalise une animation de balise "span".
 * @param {HTMLElement[]} spans
 */
export const animSpans = async (spans) => {

    for (const span of spans) {
        span.classList.add('hidden')
    }
    
    await wait(0.1)

    for (const span of shuffle(spans)) {
        animSpan1(span)
        await wait(0.1)
    }
}

/**
 * Anime UN span (et non une liste)
 * @param {HTMLElement} span 
 */
const animSpan1 = async (span) => {

    span.classList.remove('hidden')
    span.classList.add('step-0')

    await wait(0.4)
    
    span.classList.remove('step-0')
    span.classList.add('step-1')
    
    await wait(0.4)
    
    span.classList.remove('step-1')
}


