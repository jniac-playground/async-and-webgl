import { animSpans } from './anim.js'
import { splitToSpan } from './utils.js'
import './three-stage.js'

splitToSpan(document.querySelector('h1'))
splitToSpan(document.querySelector('h2'))

animSpans(document.querySelectorAll('h1 span'))
