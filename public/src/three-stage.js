import * as THREE from './three.module.js'

const canvas = document.querySelector('canvas#three')

let width = window.innerWidth
let height = window.innerHeight

canvas.width = width
canvas.height = height

const scene = new THREE.Scene()
const camera = new THREE.PerspectiveCamera(50, width / height)
const renderer = new THREE.WebGLRenderer({
    canvas,
})
renderer.setSize(width, height)
renderer.setClearColor('blue')

camera.position.z = 5

const createLights = () => {
    const ambient = new THREE.AmbientLight('white', 0.8)
    scene.add(ambient)
    const directional = new THREE.DirectionalLight('white', 0.4)
    directional.position.set(10, 10, 3)
    scene.add(directional)
    return { ambient, directional }
}

const createCube = () => {
    const geometry = new THREE.BoxGeometry()
    const material = new THREE.MeshStandardMaterial({ color:'red' })
    const mesh = new THREE.Mesh(geometry, material)
    scene.add(mesh)
    return mesh
}

createLights()
const cube = createCube()
cube.scale.set(2, 2, 2)

const loop = () => {

    requestAnimationFrame(loop)

    cube.rotation.x += 0.01
    cube.rotation.y += 0.01

    renderer.render(scene, camera)
}

loop()
